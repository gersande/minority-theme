<?php
/*
Template Name: No Sidebar Page Template
*/
?>

<?php while (have_posts()) : the_post(); ?>
	<div id="center-img-about">
	  <?php
		    // check if the post has a Post Thumbnail assigned to it.
			if ( has_post_thumbnail() ) {
				the_post_thumbnail();
			} 
	  ?>
	</div>
  <?php get_template_part('templates/page', 'header'); ?>
  <div class="front-page-text"><?php get_template_part('templates/content', 'page'); ?></div>
<?php endwhile; ?>

<hr class="separation-i text-center">

<div class="container wrap">
	<div class="row">
		<?php dynamic_sidebar( 'above-footer' ); ?>
	</div>
</div>