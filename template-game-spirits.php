<?php
/*
Template Name: Spirits of Spring Page Template
*/
?>

<?php while (have_posts()) : the_post(); ?>
  <div id="center-img">
	  <?php
	  	  // check if the post has a Post Thumbnail assigned to it.
	  	  if ( has_post_thumbnail() ) {
				the_post_thumbnail();
		  } 
  	  ?>
  </div>
  <div id="game-section-papo" class="text-center col-md-12">
	<ul>
		<?php dynamic_sidebar( 'spring-sale' ); ?>
	</ul>
  </div>
  <div class="row col-md-12">
	  <div class="col-md-6">
		<ul>
			<?php dynamic_sidebar( 'spring-trailer' ); ?>
		</ul>
	  </div>
	  <div class="col-md-6">
		  	<?php dynamic_sidebar( 'spring-images' ); ?>
	  </div>
  </div>
  <div class="col-md-6">	
	  	<!--<?php get_template_part('templates/page', 'header'); ?>-->
	  	<?php get_template_part('templates/content', 'page'); ?>
  </div>
  <div class="col-md-4">
	<?php dynamic_sidebar( 'spring-quotes' ); ?>
  </div>
<?php endwhile; ?>

<hr class="separation-i text-center">

<div class="container wrap">
	<div class="row">
		<?php dynamic_sidebar( 'above-footer' ); ?>
	</div>
</div>