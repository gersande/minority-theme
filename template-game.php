<?php
/*
Template Name: Papo Y Yo Page Template
*/
?>

<?php while (have_posts()) : the_post(); ?>
  <div id="center-img">
	  <?php
	  	  // check if the post has a Post Thumbnail assigned to it.
	  	  if ( has_post_thumbnail() ) {
				the_post_thumbnail();
		  } 
  	  ?>
  </div>
  <div id="game-section-papo" class="text-center col-md-12">
	<ul>
		<?php dynamic_sidebar( 'papo-sale' ); ?>
	</ul>
  </div>
  <div class="row col-md-12">
	  <div class="col-md-6">
		<ul>
			<?php dynamic_sidebar( 'papo-trailer' ); ?>
		</ul>
	  </div>
	  <div class="col-md-6">
		  	<?php dynamic_sidebar( 'papo-images' ); ?>
	  </div>
  </div>
  <div class="col-md-6">	
  	<!--<div class="front-page-text">-->
	  	<!--<?php get_template_part('templates/page', 'header'); ?>-->
	  	<?php get_template_part('templates/content', 'page'); ?>
	<!--</div-->
  </div>
  <div class="col-md-5">
	<ul>
		<?php dynamic_sidebar( 'papo-quotes' ); ?>
	</ul>
  </div>
<?php endwhile; ?>

<hr class="separation-i text-center">

<div class="container wrap">
	<div class="row">
		<?php dynamic_sidebar( 'above-footer' ); ?>
	</div>
</div>
<div id="humble-complete" class="humble-widget" style="display: none;">
      <div class="overlay" onclick="this.parentNode.style.display='none'"></div>  
            	<iframe class="humble-widget-inner" width="550" height="264" scrolling="no" frameborder="0" style="border:none;" src="https://www.humblebundle.com/store/product/papoandyo/Pdh6gdey5eh">
	            </iframe> 
         
</div>