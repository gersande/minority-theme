<?php
/*
Template Name: Team Page Template
*/
?>

<div class="container">
	<?php get_template_part('templates/page', 'header'); ?>
	<?php get_template_part('templates/content', 'page'); ?></div>
</div>

<?php query_posts( array( 'post_type' => 'team-members', 'posts_per_page' => 5 ) ); ?>

<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'roots'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>

<?php while (have_posts()) : the_post(); ?>
	  <div class="col-md-4 team-panels">
	    <div class="inside-panel">
		    <div class="mask"></div>
		    <div class="thumbnail-mask"> 
			  <?php
			  	  // check if the post has a Post Thumbnail assigned to it.
			  	  if ( has_post_thumbnail() ) {
						the_post_thumbnail( array(600, 460) );
				  }
		  	  ?>
		    </div>
		  	<div id="team-content-panel" class="panel-content">
			  	<h1 id="team-entry-title">
				  	<a href="<?php echo get_permalink(); ?>"><?php the_title() ;?></a>
				</h1>
				<h3 class="text-center"><?php the_meta(); ?></h3>
			  	<p style="display:inline-block;" class="pull-right" id="team-readmore"><a href="<?php echo get_permalink(); ?>">Read more...</a><p>
			</div>
		</div>
	  </div>
<?php endwhile; ?>

<?php if ($wp_query->max_num_pages > 1) : ?>
  <nav class="post-nav">
    <ul class="pager">
      <li class="previous"><?php next_posts_link(__('&larr; Previous page', 'roots')); ?></li>
      <li class="next"><?php previous_posts_link(__('Next page &rarr;', 'roots')); ?></li>
    </ul>
  </nav>
<?php endif; ?>
