<?php
/*
Template Name: Press Page Template
*/
?>

<?php while (have_posts()) : the_post(); ?>
	  <div class="container wrap">
	  	<div class="text-left clearfix special-press-title"><h1><?php echo get_the_title(); ?></h1></div>
	  	<div class="press-page-text-byline clearfix"><?php get_template_part('templates/content', 'page'); ?></div>
	  </div>
<?php endwhile; ?>

<div class="container wrap">
	<div class="row">
		<div class="col-md-4 press-col-left">
			<?php dynamic_sidebar( 'press-left' ); ?>
		</div>
		<div class="col-md-4 press-col-center">
			<?php dynamic_sidebar( 'press-center' ); ?>
		</div>
		<div class="col-md-4 press-col-right">
			<?php dynamic_sidebar( 'press-right' ); ?>
		</div>
	</div>
</div>

<br><br><br>

<?php rewind_posts(); ?>

<div class="press-time-machine-posts-roll clearfix">
	<div class="text-left clearfix special-press-title"><?php dynamic_sidebar('press-subtitle')?></div>
	<div class="container wrap">
		<div class="row">
			<div class="text-left clearfix special-press-title"><h2>TIME MACHINE</h2></div>
			
			<?php query_posts( 'category_name=time-machine-press&posts_per_page=5' ); ?>
			
			<?php if (!have_posts()) : ?>
			  <div class="alert alert-warning">
			    <?php _e('Sorry, no results were found.', 'roots'); ?>
			  </div>
			  <?php get_search_form(); ?>
			<?php endif; ?>
			
			<?php while (have_posts()) : the_post(); ?>
				  <div class="col-md-2 press-blog-panels">
				    <div class="press-inside-panel">
					    <div class="mask"></div>
					    <div class="press-thumbnail-mask"> 
						  <?php
						  	  // check if the post has a Post Thumbnail assigned to it.
						  	  if ( has_post_thumbnail() ) {
									the_post_thumbnail( array(340, 170) );
							  }
					  	  ?>
					    </div>
					  	<div class="press-panel-content">
						  	<div class="press-entry-title">
							  	<a href="<?php echo get_permalink(); ?>"><?php the_title() ;?></a>
							</div>
						</div>
					</div>
				  </div>
			<?php endwhile; ?>
		</div>
	</div>
</div>

<?php rewind_posts(); ?>

<div class="press-time-machine-posts-roll clearfix">
	<div class="container wrap">
		<div class="row">
			<div class="text-left clearfix special-press-title"><h2>PAPO &amp; YO</h2></div>
			
			<?php query_posts( 'category_name=papo-y-yo-press&posts_per_page=5' ); ?>
			
			<?php if (!have_posts()) : ?>
			  <div class="alert alert-warning">
			    <?php _e('Sorry, no results were found.', 'roots'); ?>
			  </div>
			  <?php get_search_form(); ?>
			<?php endif; ?>
			
			<?php while (have_posts()) : the_post(); ?>
				  <div class="col-md-2 press-blog-panels">
				    <div class="press-inside-panel">
					    <div class="mask"></div>
					    <div class="press-thumbnail-mask"> 
						  <?php
						  	  // check if the post has a Post Thumbnail assigned to it.
						  	  if ( has_post_thumbnail() ) {
									the_post_thumbnail( array(340, 170) );
							  }
					  	  ?>
					    </div>
					  	<div class="press-panel-content">
						  	<div class="press-entry-title">
							  	<a href="<?php echo get_permalink(); ?>"><?php the_title() ;?></a>
							</div>
						</div>
					</div>
				  </div>
			<?php endwhile; ?>
		</div>
	</div>
</div>

<?php rewind_posts(); ?>

<div class="press-time-machine-posts-roll clearfix">
	<div class="container wrap">
		<div class="row">
			<div class="text-left clearfix special-press-title"><h2>Spirits Of Spring</h2></div>
			
			<?php query_posts( 'category_name=spirits-of-spring-press&posts_per_page=5' ); ?>
			
			<?php if (!have_posts()) : ?>
			  <div class="alert alert-warning">
			    <?php _e('Sorry, no results were found.', 'roots'); ?>
			  </div>
			  <?php get_search_form(); ?>
			<?php endif; ?>
			
			<?php while (have_posts()) : the_post(); ?>
				  <div class="col-md-2 press-blog-panels">
				    <div class="press-inside-panel">
					    <div class="mask"></div>
					    <div class="press-thumbnail-mask"> 
						  <?php
						  	  // check if the post has a Post Thumbnail assigned to it.
						  	  if ( has_post_thumbnail() ) {
									the_post_thumbnail( array(340, 170) );
							  }
					  	  ?>
					    </div>
					  	<div class="press-panel-content">
						  	<div class="press-entry-title">
							  	<a href="<?php echo get_permalink(); ?>"><?php the_title() ;?></a>
							</div>
						</div>
					</div>
				  </div>
			<?php endwhile; ?>
		</div>
	</div>
</div>

<div class="press-time-machine-posts-roll clearfix">
	<div class="container wrap">
		<div class="row">
			<div class="text-left clearfix special-press-title"><h2>MINORITY MEDIA INC.</h2></div>
			
			<?php query_posts( 'category_name=minority-press&posts_per_page=5' ); ?>
			
			<?php if (!have_posts()) : ?>
			  <div class="alert alert-warning">
			    <?php _e('Sorry, no results were found.', 'roots'); ?>
			  </div>
			  <?php get_search_form(); ?>
			<?php endif; ?>
			
			<?php while (have_posts()) : the_post(); ?>
				  <div class="col-md-2 press-blog-panels">
				    <div class="press-inside-panel">
					    <div class="mask"></div>
					    <div class="press-thumbnail-mask"> 
						  <?php
						  	  // check if the post has a Post Thumbnail assigned to it.
						  	  if ( has_post_thumbnail() ) {
									the_post_thumbnail( array(340, 170) );
							  }
					  	  ?>
					    </div>
					  	<div class="press-panel-content">
						  	<div class="press-entry-title">
							  	<a href="<?php echo get_permalink(); ?>"><?php the_title() ;?></a>
							</div>
						</div>
					</div>
				  </div>
			<?php endwhile; ?>
		</div>
	</div>
</div>

<div class="container wrap">
	<div class="row">
		<div class="press-release-page-text">
			<?php dynamic_sidebar( 'press-below' ); ?>
		</div>
	</div>
</div>

<div class="container wrap">
	<div class="row">
		<?php dynamic_sidebar( 'above-footer' ); ?>
	</div>
</div>