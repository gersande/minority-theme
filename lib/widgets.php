<?php
function minority_widgets_init() {

/* MAIN PAGE WIDGETS */ 
	register_sidebar( array(
        'name' => __( 'Main Page Above Footer', 'above-footer' ),
        'id' => 'above-footer',
        'description' => __( 'This widget appears at the bottom of the main pages.', 'theme-slug' ),
        'before_title' => '<h3>',
        'after_title' => '</h3>',
        'before_widget' => '<div id="%1$s" class="col-md-6 widget-above-footer %2$s">',
		'after_widget'  => '</div>',
    ) );
    register_sidebar( array(
        'name' => __( 'Social Media Icons', 'social-media' ),
        'id' => 'social-media',
        'description' => __( 'This widget appears at the top left of the website in the navbar.', 'theme-slug' ),
        'before_title' => '<!--',
        'after_title' => '-->',
        'before_widget' => ' ',
		'after_widget'  => ' ',
    ) );
    
/* LANGUAGE WIDGETS */
    register_sidebar( array(
        'name' => __( 'Language Changing Widget', 'language-top-right' ),
        'id' => 'language-top-right',
        'description' => __( 'This widget appears at the top right of the website in the navbar. I want it to control how the language thingies are toggled.', 'theme-slug' ),
        'before_title' => '<!--',
        'after_title' => '-->',
        'before_widget' => ' ',
		'after_widget'  => ' ',
    ) );
    
/* HOME PAGE CAROUSEL */

        register_sidebar( array(
        'name' => __( 'Home Page Carousel', 'home-carousel' ),
        'id' => 'home-carousel',
        'description' => __( 'This widget controls the front carousel on the front page. To edit the carousel, change the slider from the Soliloquies Plugin.', 'theme-slug' ),
        'before_title' => '<!--',
        'after_title' => '-->',
        'before_widget' => ' ',
		'after_widget'  => ' ',
    ) );
    
/* PRESS PAGE WIDGETS*/
	register_sidebar( array(
        'name' => __( 'Press Column Left', 'press-left' ),
        'id' => 'press-left',
        'description' => __( 'This widget is for the Press Page, for the Left Column.', 'theme-slug' ),
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ) );
    register_sidebar( array(
        'name' => __( 'Press Column Middle', 'press-center' ),
        'id' => 'press-center',
        'description' => __( 'This widget is for the Press Page, for the Center Column.', 'theme-slug' ),
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ) );
    register_sidebar( array(
        'name' => __( 'Press Column Right', 'press-right' ),
        'id' => 'press-right',
        'description' => __( 'This widget is for the Press Page, for the Right Column.', 'theme-slug' ),
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ) );
    register_sidebar( array(
        'name' => __( 'Press Column Content Between Three Columns and 3 Feeds', 'press-below-three' ),
        'id' => 'press-below-three',
        'description' => __( 'This widget is for the Press Page, for the Left Column.', 'theme-slug' ),
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ) );
	register_sidebar( array(
        'name' => __( 'Press After Main Content', 'press-below' ),
        'id' => 'press-below-three',
        'description' => __( 'This widget is for the Press Page, below the Main Content.', 'theme-slug' ),
        'before_title' => '<h2>',
        'after_title' => '</h2>',
        'after_widget' => '<hr class="separation-i text-center">',
    ) );
    register_sidebar( array(
        'name' => __( 'Press Subtitle', 'press-subtitle' ),
        'id' => 'press-subtitle',
        'description' => __( 'This widget is for the Press Page, below the three main columns, where the "More On..." is controlled.', 'theme-slug' ),
        'before_title' => '<h3>',
        'after_title' => '</h3>',
        'after_widget' => ' ',
    ) );
	
/* PAPO Y YO WIDGETS*/
    register_sidebar( array(
        'name' => __( 'Papo Sale Images', 'papo-sale' ),
        'id' => 'papo-sale',
        'description' => __( 'This widget is for the Papo Y Yo sale images. For best results, use images that have a HEIGHT of 125px in a single Text/HTML widget.', 'theme-slug' ),
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ) );
    register_sidebar( array(
        'name' => __( 'Papo Trailer Video', 'papo-trailer' ),
        'id' => 'papo-trailer',
        'description' => __( 'This widget is for the Papo Y Yo trailer. Copying a video embed code into an HTML/Text widget is necessary, and for the video to resize automatically surround the embed code with the < div  class= "aspect-ration" >embedcodehere< / div > for best results.', 'theme-slug' ),
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ) );
    register_sidebar( array(
        'name' => __( 'Papo Art Images', 'papo-images' ),
        'id' => 'papo-images',
        'description' => __( 'This widget is for the Papo Y Yo images that go directly to the right of the trailer. If this widget misbehaves, please check that all images have a height of at least 291px.', 'theme-slug' ),
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ) );
	register_sidebar( array(
        'name' => __( 'Papo Quotes', 'papo-quotes' ),
        'id' => 'papo-quotes',
        'description' => __( 'This widget is for the Papo Y Yo quotes that go directly to the right of the trailer, under the images.', 'theme-slug' ),
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ) );
    
/* SPIRITS OF SPRING WIDGETS*/
	register_sidebar( array(
        'name' => __( 'Spirits of Spring Sale Images', 'spring-sale' ),
        'id' => 'spring-sale',
        'description' => __( 'This widget is for the Spirits of Spring images that go directly to the right of the trailer. Using a custom gallery embed code is best.', 'theme-slug' ),
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ) );
    register_sidebar( array(
        'name' => __( 'Spirits of Spring Trailer', 'spring-trailer' ),
        'id' => 'spring-trailer',
        'description' => __( 'This widget is for the Spirits of Spring trailer. Copying a video embed code into an HTML/Text widget is necessary, and for the video to resize automatically surround the embed code with the < div  class= "aspect-ration" >embedcodehere< / div > for best results.', 'theme-slug' ),
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ) );
    register_sidebar( array(
        'name' => __( 'Spirits of Spring Images', 'spring-images' ),
        'id' => 'spring-images',
        'description' => __( 'This widget is for the Spirits of Spring images that go directly to the right of the trailer. If this widget misbehaves, please check that all images have a height of at least 291px.', 'theme-slug' ),
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ) );
    register_sidebar( array(
        'name' => __( 'Spirits of Spring Quotes', 'spring-quotes' ),
        'id' => 'spring-quotes',
        'description' => __( 'This widget is for the Spirits of Spring quotes that go directly to the right of the trailer, under the images.', 'theme-slug' ),
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ) );
/* LOCO MOTORS WIDGETS*/
    register_sidebar( array(
        'name' => __( 'Loco Motors Sale Images', 'loco-sale' ),
        'id' => 'loco-sale',
        'description' => __( 'This widget is for the Loco Motors sale images. For best results, use images that have a HEIGHT of 125px in a single Text/HTML widget. ', 'theme-slug' ),
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ) );
    register_sidebar( array(
        'name' => __( 'Loco Motors Trailer Video', 'loco-trailer' ),
        'id' => 'loco-trailer',
        'description' => __( 'This widget is for the Loco Motors trailer. Copying a video embed code into an HTML/Text widget is necessary, and for the video to resize automatically surround the embed code with the < div  class= "aspect-ration" >embedcodehere< / div > for best results.', 'theme-slug' ),
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ) );
    register_sidebar( array(
        'name' => __( 'Loco Motors Art Images', 'loco-images' ),
        'id' => 'loco-images',
        'description' => __( 'This widget is for the Loco Motors images that go directly to the right of the trailer. If this widget misbehaves, please check that all images have a height of at least 291px.', 'theme-slug' ),
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ) );
	register_sidebar( array(
        'name' => __( 'Loco Motors Quotes', 'loco-quotes' ),
        'id' => 'loco-quotes',
        'description' => __( 'This widget is for the Loco Motors quotes that go directly to the right of the trailer, under the images.', 'theme-slug' ),
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ) );

/* TIME MACHINE WIDGETS*/
    register_sidebar( array(
        'name' => __( 'Time Machine Sale Images', 'time-sale' ),
        'id' => 'time-sale',
        'description' => __( 'This widget is for the Time Machine sale images. For best results, use images that have a HEIGHT of 125px in a single Text/HTML widget. ', 'theme-slug' ),
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ) );
    register_sidebar( array(
        'name' => __( 'Time Machine Trailer Video', 'time-trailer' ),
        'id' => 'time-trailer',
        'description' => __( 'This widget is for the Time Machine trailer. Copying a video embed code into an HTML/Text widget is necessary, and for the video to resize automatically surround the embed code with the < div  class= "aspect-ration" >embedcodehere< / div > for best results.', 'theme-slug' ),
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ) );
    register_sidebar( array(
        'name' => __( 'Time Machine Art Images', 'time-images' ),
        'id' => 'time-images',
        'description' => __( 'This widget is for the Time Machine images that go directly to the right of the trailer. If this widget misbehaves, please check that all images have a height of at least 291px.', 'theme-slug' ),
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ) );
	register_sidebar( array(
        'name' => __( 'Time Machine Quotes', 'time-quotes' ),
        'id' => 'time-quotes',
        'description' => __( 'This widget is for the Time Machine quotes that go directly to the right of the trailer, under the images.', 'theme-slug' ),
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ) );
    
}

add_action( 'widgets_init', 'minority_widgets_init' );