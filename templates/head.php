<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="alternate" type="application/rss+xml" title="<?php echo get_bloginfo('name'); ?> Feed" href="<?php echo esc_url(get_feed_link()); ?>">
  <?php wp_head(); ?>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

  <!-- Google Fonts --> 
  <link href='http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <!-- FONT AWESOME -->
  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
  <style>
	  /* FONTS */
		
		@font-face { 
			font-family: Gibson; 
			src: url('<?php echo get_template_directory_uri(); ?>/assets/fonts/Gibson-Regular.ttf'); 
		}
		@font-face {
			font-family: GibsonBig;
			src: url('<?php echo get_template_directory_uri(); ?>/assets/fonts/Gibson-Bold.ttf');
		} 
  </style>
  <script>
	  
  </script>
</head>
