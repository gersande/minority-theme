<div class="text-center">
<img class="text-center lost-image" src="<?php echo get_template_directory_uri(); ?>/assets/img/pretty_papo.jpg" alt="Papo & Yo Screenshot">
<?php get_template_part('templates/page', 'header'); ?>
</div>
<div class="front-page-text">	
	<div class="alert alert-danger">
	  <?php _e('Sorry, but the page you were trying to view does not exist.', 'roots'); ?>
	</div>
	
	<p><?php _e('It looks like this was the result of either:', 'roots'); ?></p>
	<ul>
	  <li><?php _e('a mistyped address', 'roots'); ?></li>
	  <li><?php _e('an out-of-date link', 'roots'); ?></li>
	</ul>
	<br>
	<h3>Try Searching The Minority Website:</h3><?php get_search_form() ?>
</div>