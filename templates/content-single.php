<?php while (have_posts()) : the_post(); ?>
  <article <?php post_class(); ?>>
    <header>
      <h1 class="entry-title"><?php the_title(); ?></h1>
      <?php get_template_part('templates/entry-meta'); ?>
   	</header>
	<div class="entry-content">
      <?php the_content(); ?>
    </div>
    <footer>
      <?php wp_link_pages(array('before' => '<nav class="page-nav"><p>' . __('Pages:', 'roots'), 'after' => '</p></nav>')); ?>
    </footer>
    
	<div class="sharing-buttons-bottom-single-post">
		 	 <!--FACEBOOK BUTTON--> <div class="fb-share-button" style="vertical-align: top !important" data-href="<?php echo get_permalink(); ?>" data-layout="button_count"></div> <!--TWITTER BUTTON--> <a href="https://twitter.com/share" class="twitter-share-button" data-via="we_are_minority">Tweet</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script> <!--LINKEDIN BUTTON--> <script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: en_US</script>
<script type="IN/Share" data-counter="right"></script> <!--REDDIT BUTTON--> <script type="text/javascript" src="//www.redditstatic.com/button/button1.js"></script> <!--TUMBLR BUTTON--><a class="tumblr-share-button" data-color="black" data-notes="right" href="https://embed.tumblr.com/share"></a>
<script>!function(d,s,id){var js,ajs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://secure.assets.tumblr.com/share-button.js";ajs.parentNode.insertBefore(js,ajs);}}(document, "script", "tumblr-js");</script>
	</div>	
	
    <?php comments_template('/templates/comments.php'); ?>
    
  </article>
<?php endwhile; ?>
