<?php
/*
Template Name: Home Page Template
*/
?>

<!--

Carousel	

-->
<div class="container wrap" style="margin-bottom: 50px;">
		<?php dynamic_sidebar( 'home-carousel' ); ?>
</div>

<?php while (have_posts()) : the_post(); ?>
  <div class="text-center"><?php get_template_part('templates/page', 'header'); ?></div>
  <div class="front-page-text"><?php get_template_part('templates/content', 'page'); ?></div>
<?php endwhile; ?>

<hr class="separation-i text-center">

<div class="container homepage wrap">
	<div class="row">
		<?php dynamic_sidebar( 'above-footer' ); ?>
	</div>
</div>