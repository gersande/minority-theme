<?php
/**
 * Clean up the_excerpt()
 */
function roots_excerpt_more() {
  return ' &hellip;';
}
add_filter('excerpt_more', 'roots_excerpt_more');

function roots_2_excerpt_length( $length ) {
	return 10;
}
add_filter( 'excerpt_length', 'roots_2_excerpt_length' );