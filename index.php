<div class="container"><?php get_template_part('templates/page', 'header'); ?></div>

<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'roots'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>

<?php while (have_posts()) : the_post(); ?>
	  <div class="col-md-4 blog-panels">
	    <div class="inside-panel">
		    <div class="mask"></div>
		    <div class="thumbnail-mask"> 
			  <?php
			  	  // check if the post has a Post Thumbnail assigned to it.
			  	  if ( has_post_thumbnail() ) {
						the_post_thumbnail( array(600, 300) );
				  }
		  	  ?>
		    </div>
		  	<div class="panel-content">
			  	<h2 class="entry-title">
				  	<a href="<?php echo get_permalink(); ?>"><?php the_title() ;?></a>
				</h2>
			  	<p><time>Published on <?php the_time('F j, Y'); ?></time><br><?php the_excerpt(); ?></p><p style="display:inline-block;" class="pull-right"><a href="<?php echo get_permalink(); ?>"> Read More...</a><p>
			</div>
		</div>
	  </div>
<?php endwhile; ?>

<?php if ($wp_query->max_num_pages > 1) : ?>
  <nav class="post-nav">
    <ul class="pager">
      <li class="previous" style="font-size: 28px;"><?php next_posts_link(__('&larr;', 'roots')); ?></li>
      <li class="next" style="font-size: 28px;"><?php previous_posts_link(__('&rarr;', 'roots')); ?></li>
    </ul>
  </nav>
<?php endif; ?>