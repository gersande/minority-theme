<hr class="separation-i text-center">

<footer class="content-info" role="contentinfo">
  <div class="container">
    <div class="row">
       <div class="col-md-12">
	       <?php dynamic_sidebar('sidebar-footer'); ?>
       </div>
    </div>
    <hr class="separation-i text-center">
    <div class="bottom-social text-center">
		<?php wp_nav_menu( array( 'theme_location' => 'social-header-menu', 'menu_class' => 'navbar-nav nav list-inline' ) ); ?>
	</div>
    <p class="text-center" id="copy">&copy; 2011-<?php echo date('Y'); ?> <a href="<?php echo esc_url(home_url('/')); ?>"><?php bloginfo('name'); ?></a></p>
  </div>
</footer>
