<div class="container wrap">	
	<header class="banner">
		<nav class="navbar">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/Minority_OnBlack.png" height="42px" width="auto"></a>
		    </div>
		
		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" role="navigation">
		      <?php
		        if (has_nav_menu('primary_navigation')) :
		          wp_nav_menu(array('theme_location' => 'primary_navigation', 'walker' => new Roots_Nav_Walker(), 'menu_class' => 'nav navbar-nav'));
		        endif;
		      ?>
		       <ul class="nav navbar-nav navbar-right"> 
		        <?php dynamic_sidebar( 'language-top-right' ); ?>
		      </ul>
		      <?php wp_nav_menu( array( 'theme_location' => 'social-header-menu', 'menu_class' => 'navbar-right nav-right navbar-nav nav list-inline' ) ); ?>
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
	</header>
</div>
