<?php
function minority_menus_init() {
	function register_my_menu() {
	   register_nav_menu('social-header-menu',__( 'Social Media Header Menu' ));
	}
	add_action( 'init', 'register_my_menu' );
}

add_action( 'widgets_init', 'minority_menus_init' );